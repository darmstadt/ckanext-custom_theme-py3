# Customizing CI

This documentation describes the specification of a first version of a CKAN CI Theme for Berlin Tegel Project.

Since the default CKAN CSS style sheet contains 1100 lines of code, the customizing of colors 
and font sizes were reduced to a basic theming.

## CKAN Config Parameters

For this extension the following ckan config needs to be set:

```bash
# path to custom logo
ckan.site_logo="/FUTR_HUB_RGB_SZ_BLUE.jpg"
# style to use for hompage
homepage_style="berlintxl"
# path to favicon
ckan.favicon="/favicon.ico"
```

## Images (Logo, Favicon, Hero-Image)

- **Logo:** ![FUTR_HUB_RGB_SZ_BLUE](../ckanext/custom_theme/public/FUTR_HUB_RGB_SZ_BLUE.jpg)
- **Favicon:** Favicon (16x16 format ico) ![favicon](../ckanext/custom_theme/public/favicon.ico) was created from the following Logo taking only the hook in front of the text: ![FUTR_HUB_RGB_ARROW_BLUE_IUD](images/FUTR_HUB_RGB_ARROW_BLUE_IUD.jpg)

- Image to be shown in the introduction text box (scaled to width="420" height="220"): ![Netzbild_CM](../ckanext/custom_theme/public/Netzbild_CM.png)
- default background image was removed

## CSS - style sheet (basic colors, fonts, font sizes)

### Basic Style Sheet Definitions

- **basic colors**:
  - **dark grey**: rgb(169,170,172)
  - **silver**: rgb(218, 224, 227)
  - **light blue:** rgb(33,147,201)
  - **dark blue:** rgb(21, 113, 170)
- **font**: Google Font "Lato"
- **base font size**: 16px (other sizes were not adjusted yet)

### Masterheader (Login, Registration, etc.)

- **background color**: dark blue
- **hover color**: dark gray
- **border left color**: dark gray
- **font-weight**: 600

**Previews:**

The masterheader looks like this at the first start:
![Masterheader not logged in](images/masterheader2.png)

If the ckan_admin is logged in the masterheader looks like this:
![Masterheader logged in as ckan_admin](images/masterheader1.png)


### Header (Navigation items, Logo)

- **Content:**
  - **Logo**: 
    - **max-height**: 120px
  - **Menu Items**: 
    - Datensätze (default)
    - Organisationen (default)
    - Gruppen (default)
    - Über uns (default)
  - **Search Box**
- **Theming Navigation Bar/Menu**
  - **button background color**: light blue
  - **button text color**: white
  - **button hover colors**:
    - **text color**: white
    - **background color**: dark blue
  - **button active colors**:
    - **text color**: white
    - **background color**: dark blue
  - **navigation background color**: light blue
  - **min-height**: 160px (to have more space for logo)

**Previews**:

Header:
![header](images/header.png)

Header with Menu Item "Datensätze" selected:
![header](images/header2.png)


### Footer

- **Content:**
  - **remove links**:
    - About Link
    - CKAN API
    - CKAN Association
  - **add custom links**:
    - Impressum
    - Nutzungsbedingungen
    - Datenschutz
    - Barrierefreiheit
    - Tegel Project GmbH with link to https://www.berlintxl.de/
- **Theming**
  - **link colors:** white
  - **text color:** white
  - **hover effect:** underline
  - **background color:** light blue
  - **extra text and link color for Tegel Projekt GmbH**: black

**Previews**:

The Footer looks like this:
![header](images/footer.png)

**Home Layout (Main Content and Layout of CKAN Start Site)**

- **Content:**
  - hero section removed
  - background image removed
  - layout splitted in two columns:
    - **right column**:
      - statistics box
      - search box
      - featured_organization box
    - **left column**:
      - box with introduction/welcome text and image:
```html
<h4 class="page-heading">FUTR HUB – Das Kompetenzzentrum für urbane Daten</h4>
<p>Die intelligente Digitalisierung von Städten verbessert die Lebensqualität, schont Ressourcen und macht urbanes Wohnen und Wirtschaften nachhaltiger. Berlin TXL soll für die deutsche Hauptstadt ein Vorreiter auf dem Weg zu einer Smart City werden, dafür brauchen die smarten Quartiere von Berlin TXL nicht nur eine digitale Infrastruktur, sondern auch Orte für Innovationen und ein Partner-Netzwerk für die sinnvolle und sichere Nutzung von urbanen Daten. Entwickelt wird die digitale Infrastruktur für Berlin TXL und Smart Cities weltweit im FUTR HUB, dem Kompetenzzentrum für urbane Daten.</p>
<h4 class="page-heading">Kern des FUTR HUB: Die Plattform für urbane Daten</h4>
<p>Zentrale Infrastruktur des FUTR HUB ist eine Datenplattform, die urbane Daten zusammenführen, vernetzen und bereitstellen wird. Urbane Daten können zum Beispiel aus Sensoren in Gebäudelüftungen, Ampeln oder Ladesäulen stammen. Basierend auf einer Geodaten-Infrastruktur, welche z. B. die Planungsunterlagen für Berlin TXL beinhaltet, wird das System schrittweise mit einem IoT-System zusammengeführt und zu einer vollumfänglichen Daten-Plattform zu Themen wie Energie, Mobilität und Smart Nature weiterentwickelt.</p>
<p>Dies ist der Metadatenkatalog der Tegel Projekt GmbH, also die Sammlung von Daten über Daten, in der wir sowohl die Metadaten interner Daten abbilden, aber auch urbane Daten der Öffentlichkeit bereitstellen.</p>
```
  
- **Theming**:
  - **background color**: silver
  - **statistics box**:
    - **text color**: light blue
    - **hover color**: dark blue
    - **font weight**: 400
  - **search box**:
    - **text color**: white
    - **background color**: light blue
    - **tags_panel**:
      - **background color**: dark blue
      - **tags buttons**:
        - **background color:**
        - **hover color**:
        - **text color**:
  - **featured_organization box**:
    - no customizing yet, default colors were used
  - **introduction box**:
    - **text color**: black

**Previews**:

The main content of the startside looks like this:
![header](images/start_side_theming.png)

### further general elements

**Theming**:

- **pagination**:
  - **buttons:**
    - **background color active**: dark blue
    - **text-color active**: dark blue
- **stages** (input form 1. dataset, 2. resource)
  - **active stage**:
    - number: 
      - **background color**: white
      - **text color**: dark blue
    - text: 
      - **background color**: dark blue
      - **text color**: white
  - **inactive stage**:
    - number: 
      - **background color**: silver
      - **text color**: light blue
    - text: 
      - **background color**: light blue
      - **text color**: silver

- **tags filter**:
  - **background color**: dark blue
  - **text color**: white

- **badges** (Number of found datasets):
  - **background colors**: dark grey
  - **text color**: white

- **pills**:
  - **background color**: dark blue
  - **color**: white

- **buttons**:
  - **background color**: light blue
  - **border-color**: dark blue
  - **text color**: white
  - **hover background color**: dark blue

- **tables** (e.g. additional info page):
  - **table headers**:
    - **background color**: dark blue
    - **text color**: white

- **select box** (e.g. language selector drop down):
  - **highlighted background color**: dark blue
